package com.example.userservice;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import qt3.music.domains.User;

@Repository
public interface UserRepository extends ReactiveMongoRepository<User, String> {
}
