package com.example.userservice;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import qt3.music.domains.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final UserRepository userRepository;

    public Flux<User> getAllUsers() {
        return userRepository.findAll();
    }

    public Mono<User> saveUser() {
        User user = new User(UUID.randomUUID().toString(), "fisrtanem", "lastname", "gender", "emial");
        Mono<User> save = userRepository.save(user);
        log.info("user {} is saved", user.getId());
        return save;
    }
}
