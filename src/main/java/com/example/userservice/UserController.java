package com.example.userservice;

import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import qt3.music.domains.User;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;

@RestController
@RequestMapping("/")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("all-users")
    public Flux<User> getAllUsers() {
        return userService.getAllUsers()
                .delaySubscription(Duration.ofSeconds(0));
    }

    @GetMapping("create-user")
    public Mono<User> saveUser(/*@RequestHeader("Authorization") String token*/) {
        return userService.saveUser();
    }
}
