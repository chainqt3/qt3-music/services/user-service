package com.example.userservice;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.data.mongodb.repository.config.EnableReactiveMongoRepositories;
import org.springframework.web.reactive.function.client.WebClient;
import qt3.music.domains.User;
import reactor.core.publisher.Flux;

import java.util.UUID;
import java.util.stream.Stream;

@SpringBootApplication
@EnableReactiveMongoRepositories
public class UserServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class, args);
    }

    @Bean
    @LoadBalanced
    WebClient.Builder client() {
        return WebClient.builder();
    }

    @Bean
    ApplicationRunner applicationRunner(UserRepository userRepository) {
        return args -> {
            User user1 = new User(UUID.randomUUID().toString(), "firstname1", "lastname", "", "email");
            User user2 = new User(UUID.randomUUID().toString(), "firstname2", "lastname", "", "email");
            User user3 = new User(UUID.randomUUID().toString(), "firstname3", "lastname", "", "email");
            User user4 = new User(UUID.randomUUID().toString(), "firstname4", "lastname", "", "email");

            Stream.of(user1, user2, user3, user4).forEach(user -> userRepository.save(user).subscribe());
        };
    }
}
